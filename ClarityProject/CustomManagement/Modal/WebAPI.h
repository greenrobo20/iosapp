//
//  WebAPI.h
//Created By Gurmeet Kaur
//#ifndef PI_Investigation_API_S_h
#ifndef WebAPI_h
#define WebAPI_h
#define mainScreenWidth    [[UIScreen mainScreen] bounds].size.width
#define mainScreenHeight   [[UIScreen mainScreen] bounds].size.height
#define AppName       @"Clarity Project"
#define InternetFailureMessage @"Something went wrong.Please try again or check your internet connection."

#pragma mark - @"Clarity Project":
//#define   BaseURL       @"http://www.intellectappstudioz.com/winloss/index.php/auth"

#define   BaseURL_Head       @"http://demo.bigserver.in/projectshivam/superadmin/apis"

#define   view_MarketingHeadActivity     BaseURL_Head@"/viewmarketinghead_activity"

#define   view_MarketingHead       BaseURL_Head@"/view_marketinghead"


#define   Login_Head               BaseURL_Head@"/loginapi"
#define   Dashboard_Cashier        BaseURL_Head@"/dashboard_cashier"
#define   Dashboard_Head           BaseURL_Head@"/dashboard_marketinghead"
#define   Dashboard_Agent          BaseURL_Head@"/dashboard_marketingagent"
#define   AddClient                BaseURL_Head@"/addclient_api"
#define   AddCashAPI               @"http://demo.bigserver.in/projectshivam/superadmin/apis/addcash_api"
#define   View_Cash_API            BaseURL_Head@"/viewcash_api"
#define  View_MarketingHead_Activity    BaseURL_Head@"/viewmarketinghead_activity"
#define   Cashier_agent_api            BaseURL_Head@"/agentcashier_api"

#define   Cashier_approve_api            BaseURL_Head@"/cashierapprovied_appi"
#define   Manager_Client_Activity       BaseURL_Head@"/manager_clientactivities"


#define   Agent_Cashier_API        BaseURL_Head@"/agentcashier_api"
#define   update_Agent_Cashier     BaseURL_Head@"/agentcashier_api"
#define   viewClient                 BaseURL_Head@"/viewclient_api"
#define   cashierApproved_API      BaseURL_Head@"/cashierapproved_api"
#define   agentcashier_API         BaseURL_Head@"/agentcashier_api"

#define marketingHead_Agent        BaseURL_Head@"/marketinghead_agentactivities"
#define marketingHead_Client       BaseURL_Head@"/marketinghead_clientactivities"


//manager_API

#define manager_dashboard   BaseURL_Head@"/manager_dashboard"
#define  manager_Marketing_Agent      BaseURL_Head@"/view_marketingagent"
#define  manager_Marketing_Agent_Activity      BaseURL_Head@"/viewmarketingagent_activity"
#define  manager_Marketing_Head      BaseURL_Head@"/view_marketinghead"
#define  manager_Marketing_Head_Activity      BaseURL_Head@"/viewmarketinghead_activity"
#define manage_head_Activity              BaseURL_Head@"/manager_head_agent_api"




#define   AcceptReject_Friends        @"h​ttp://www.linkception.com/winloss/index.php/userfriends/confirmRequest"
#define   create_Post                 @"http://www.linkception.com/winloss/index.php/posts/create"
#define   upload_Video_User           @"http://www.linkception.com/winloss/index.php/auth/uploadvideo"
#define   upload_Photo_User           @"http://www.linkception.com/winloss/index.php/auth/uploadprofileimage"
#define   upload_profilePic           @"http://www.linkception.com/winloss/index.php/auth/uploadprofileimage"
#define   edit_profile                @"http://www.linkception.com/winloss/index.php/auth/edituserprofile"
#define   settingScreen               @"http://www.linkception.com/winloss/index.php/auth/settings"
#define   discovery_List              @"http://www.linkception.com/winloss/index.php/auth/discovery"
#define   addFriend_List              @"http://www.linkception.com/winloss/index.php/userfriends/addFriend"
#define   getFriend_ListNew           @"http://www.linkception.com/winloss/index.php/auth/getfriends"
#define   getAllPostForHome           @"http://www.linkception.com/winloss/index.php/posts/getAllPostsForHomeScreen"
//#define   getAllPostForHome         @"http://www.linkception.com/winloss/index.php/posts/homescreenposts"
#define   PhotoLoad                   @"http://www.linkception.com/winloss/"
#define   Friend_Request_API          @"http://www.linkception.com/winloss/index.php/userfriends/friendRequests"
#define   post_Comment                @"h​ttp://www.linkception.com/winloss/index.php/posts/addcomment"
#define   Assigned_Received_Post      @"http://www.linkception.com/winloss/index.php/posts/assignedReceivedPosts"
#define   getAllPostsByUserr          @"h​ttp://www.linkception.com/winloss/index.php/posts/getAllPosts"
#define   OTP_API                     @"h​ttp://www.linkception.com/winloss/index.php/auth/otpVerification"
#endif

