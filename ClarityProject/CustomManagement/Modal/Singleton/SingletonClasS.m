

#import "SingletonClasS.h"


@implementation SingletonClasS{
    //AppDelegate * appDel;
    UIView      * hview;
    UIView * backgraoungVC;
    NSDictionary* json;
}
+(SingletonClasS *)sharedInstance{
   
    static SingletonClasS *singleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singleton = [[self alloc] init];
      
    });
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(clickRemoveBackground) name:@"removeBackground" object:self];
    return singleton;
}

-(void)getStoredValue{
    _isOwner = false;
    // _obj=[[UserObj alloc]init];
    // _ScheduleObj=[[ScheduleBookingObject alloc]init];
    // _obj1=[[OwnerObjPark alloc]init];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isOwner"]){
        _isOwner = [[NSUserDefaults standardUserDefaults] boolForKey:@"isOwner"];
    }
}

//-(void)settingScreen{
//    _settingVC = false;
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"FaceBookBtn"]){
//        _settingVC = [[NSUserDefaults standardUserDefaults] boolForKey:@"FaceBookBtn"];
//    }
//    
//}
#pragma mark - hideKeyboard

-(void)tap1:(UIView *)view{
    
    view1=view;
    UITapGestureRecognizer  *tap = [[UITapGestureRecognizer alloc]
                                    initWithTarget:self
                                    action:@selector(dismissKeyboard)];
    [view1 addGestureRecognizer:tap];
    
}
-(void)dismissKeyboard {
    [view1 endEditing:YES];
}
#pragma mark - Validate Email With String

- (BOOL)validateEmailWithString:(NSString*)email {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}
#pragma mark - IsPasswordValid
#pragma mark -
-(BOOL) isPasswordValid:(NSString *)pwd {
    NSCharacterSet *upperCaseChars = [NSCharacterSet characterSetWithCharactersInString:@"ABCDEFGHIJKLKMNOPQRSTUVWXYZ"];
    //NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    if ( [pwd length]<=7|| [pwd length]>20 )
        return NO;  // too long or too short
    NSRange rang;
    rang = [pwd rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet]];
    if ( !rang.length )
        return NO;  // no letter
    //gurmeet  rang = [pwd rangeOfCharacterFromSet:upperCaseChars];
    if ( !rang.length )
        return NO;  // no uppercase letter;
    rang =[pwd rangeOfCharacterFromSet:[[NSCharacterSet alphanumericCharacterSet] invertedSet]];
    if (!rang.length)
        return NO; // no special character
    
    return YES;
}
#pragma mark - Show Alert With Action
#pragma mark -
-(void)showAlertWithAction:(UIAlertController *) alert vc : (UIViewController*)vc message:(NSString*)msg completionBlock:(voidCompletion)completionBlock secCompletion : (voidSecCompletion)SecCompletion{
    
    alert = [UIAlertController alertControllerWithTitle:
             @"Clarity Project" message:msg preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* yesButton  = [UIAlertAction actionWithTitle:@"Yes"
                                                         style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                             //    [alert dismissViewControllerAnimated:YES completion:nil];
                                                             completionBlock();
                                                         }];
    UIAlertAction* noButton  = [UIAlertAction actionWithTitle:@"No"
                                                        style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                            [alert dismissViewControllerAnimated:YES completion:nil];
                                                            SecCompletion();
                                                        }];
    [alert addAction:yesButton];
    [alert addAction:noButton];
    
    [vc presentViewController:alert animated:YES completion:nil];
}

#pragma mark - Show Alert
#pragma mark -
-(void)showSimpleAlert :(UIViewController*)vc message:(NSString*)msg completionBlock:(voidCompletion)completionBlock secCompletion : (voidSecCompletion)SecCompletion{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Collection Management" message:msg
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    //    UIAlertAction *actionOk            = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    
    UIAlertAction* actionOk  = [UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                            //    [alert dismissViewControllerAnimated:YES completion:nil];
                                                            completionBlock();
                                                        }];
    
    //You can use a block here to handle a press on this button
    [alertController addAction:actionOk];
    [vc presentViewController:alertController animated:YES completion:nil];
}

-(void)showAlert:(UIViewController *)vc message:(NSString *)msg{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Clarity Project" message:msg
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actionOk            = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    //You can use a block here to handle a press on this button
    [alertController addAction:actionOk];
    [vc presentViewController:alertController animated:YES completion:nil];
}

#pragma MARK - MBProgress

-(void)showMBProgressHud:(UIView*)myView msg:(NSString*)msg{
    [MBProgressHUD hideHUDForView:myView animated:YES];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:myView animated:YES];
    //gurmeet
    
    //hud.mode = MBProgressHUDModeAnnularDeterminate;
    hud.labelText = msg;
    hud.removeFromSuperViewOnHide = true;
}

-(void)hideMBProgressHud:(UIView*)myView{
    [MBProgressHUD hideHUDForView:myView animated:YES];
}
-(NSString*)checkResponseForString:(NSDictionary*)dict key:(NSString*)key{
    if ([dict valueForKey:key] && [[dict valueForKey:key] isKindOfClass:[NSString class]])
        return [dict valueForKey:key];
    else if ([dict valueForKey:key] && [[dict valueForKey:key] isKindOfClass:[NSNumber class]])
        return [NSString stringWithFormat:@"%@",[dict valueForKey:key]];
    return @"";
}

-(void)showToast:(UIView*)myView msg:(NSString*)msg{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:myView animated:YES];
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.label.text = msg;
    hud.margin = 10.f;
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:2];
}

#pragma mark- InternetConnection
+ (BOOL)isInternetConnected {
    // NSLog(@"%d",[AFNetworkReachabilityManager sharedManager].networkReachabilityStatus);
    return [AFNetworkReachabilityManager sharedManager].networkReachabilityStatus;
}

-(void)showNetworkView{
    if (_networkView == nil){
        _networkView = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, mainScreenWidth, 64)];
        _networkView.text = @"No internet connection";
        _networkView.backgroundColor =[UIColor colorWithRed:236/255.0 green:62/255.0 blue:50/255.0 alpha:1.0];
        _networkView.textColor = [UIColor whiteColor];
        _networkView.textAlignment = NSTextAlignmentCenter;
        _networkView.font = [UIFont systemFontOfSize:15 weight:UIFontWeightSemibold];
        [[UIApplication sharedApplication].keyWindow addSubview:_networkView];
    }
    _networkView.hidden = false;
    [[UIApplication sharedApplication].keyWindow bringSubviewToFront:_networkView];
}

-(void)hideNetworkView{
    if (_networkView){
        _networkView.hidden = true;
    }
}


#pragma mark - API handler

#pragma mark - POST
-(void)postMethod:(UIViewController*)vc url:(NSString *)strUrl dict:(NSDictionary *)dict completionBlock:(myCompletion)completionBlock
{
    if([SingletonClasS isInternetConnected]){
        [self hideNetworkView];
    }else{
        // [[SingletonClass sharedInstance]hideMBProgressHud:vc.view];
        [self showNetworkView];
        completionBlock(nil);
        return;
    }
    
    NSLog(@"dict %@",dict);
    
    //      NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:nil];
    //        NSString *json = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    //        NSDictionary *dict1 = @{@"body":json};
    
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
     manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:3600];
 //  [manager.requestSerializer setValue:@"" forHTTPHeaderField:@"auth_token"];
     //[manager.requestSerializer setValue:@"gsingh" forHTTPHeaderField:@"Username"];
     //[manager.requestSerializer setValue:@"123456" forHTTPHeaderField:@"Password"];

  
//    NSString *urlUser = [NSString stringWithFormat:@"https://mySubdomain.zendesk.com/api/v2/users/1038194884/tickets/requested.json"];
//    NSURL *url  = [[NSURL alloc] initWithString:urlUser];
//
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//
    [manager.requestSerializer setValue : @"application/json" forHTTPHeaderField : @"Content-Type" ];
    
    NSString *str_username = [[NSUserDefaults standardUserDefaults] valueForKey:@"set_Username"];
      NSString *str_password = [[NSUserDefaults standardUserDefaults] valueForKey:@"set_Password"];
    NSString *authStr =[NSString stringWithFormat:@"%@:%@",str_username,str_password];
    
   // NSString *str_auth = [NSString stringWithFormat:@"Basic %@",];
    
    NSData *authData = [authStr dataUsingEncoding:NSUTF8StringEncoding];
    
    //final String Auth="Basic" + Base64.encodeToString((Utils.getPreference(context).getString("Username","")+":"+ Utils.getPreference(context).getString("password","")).getBytes(),Base64.NO_WRAP);

    NSString *authValue = [NSString stringWithFormat: @"Basic %@",[authData base64EncodedStringWithOptions:0]];
  //  [request setValue:authValue forHTTPHeaderField:@"Authorization"];
    [[NSUserDefaults standardUserDefaults]setValue:authValue forKey:@"set_AuthValue"];
      [manager.requestSerializer setValue:authValue forHTTPHeaderField:@"Authorization"];
    
    
   // [manager.requestSerializer setValue:@"XpjGlzt1MCsuR0AMGK8AF9Oo2a7GCq9F" forHTTPHeaderField:@"x-api-key"];
    NSLog(@"%@",strUrl);
    [manager POST:strUrl parameters:dict progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError* error;
        json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions
                                                 error:&error];
        NSLog(@"Success %@",json);
        [[SingletonClasS sharedInstance]hideMBProgressHud:vc.view];
        
        completionBlock(responseObject);
    }
     
          failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             NSString *str = [[NSString alloc] initWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
              
              NSLog(@"Error ->%@", str);
              
              NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
              id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
           
              NSLog(@"%@",[json valueForKey:@"httpStatus"]);
              if([[json valueForKey:@"httpStatus"] isEqualToString:@"401"])
              {
                  [[SingletonClasS sharedInstance]showAlert:vc message:@"The username & password combination you entered is invalid."];
              }
              
              NSLog(@"error.description==%@", error.description);
              
              completionBlock(nil);
              [[SingletonClasS sharedInstance]hideMBProgressHud:vc.view];
              @try {
                  if (error.code == -1001){
                      [[SingletonClasS sharedInstance]showAlert:vc message:@"Internet connection is slow. Please try after sometime."];
                  }else if (error.code == -1005){
                      [[SingletonClasS sharedInstance]showAlert:vc message:@"The network connection was lost. Please try after sometime."];
                  }else if (error.code == 1009){
                      [[SingletonClasS sharedInstance]showAlert:vc message:@"The Internet connection appears to be offline."];
                  }
                  else if (error.code == -1011){
                      NSData *data = [str dataUsingEncoding:NSUTF8StringEncoding];
                      id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                      NSLog(@"%@",[json objectForKey:@"error_message"]);
                      [[SingletonClasS sharedInstance]showAlert:vc message:CHECK_NULL_STRING([json objectForKey:@"error_message"])];
                  }
                  else if (error.code != -999){
                      [[SingletonClasS sharedInstance]showAlert:vc message:InternetFailureMessage];
                  }
                  
              } @catch (NSException *exception) {
                  NSLog(@"Unable to show pop up");
              }
              
          }];
    
}

#pragma mark - GET

-(void)getMethod:(UIViewController*)vc url:(NSString *)strUrl dict:(NSDictionary *)dict completionBlock:(myCompletion)completionBlock
{
    
    if([SingletonClasS isInternetConnected]){
        [self hideNetworkView];
    }else{
        // [[SingletonClass sharedInstance]hideMBProgressHud:vc.view];
        [self showNetworkView];
        completionBlock(nil);
        return;
    }
    
    NSLog(@"dict %@",dict);
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"set_AuthValue"]);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    [manager.requestSerializer setTimeoutInterval:3600];
      [manager.requestSerializer setValue : @"application/json" forHTTPHeaderField : @"Accept" ];
    [manager.requestSerializer setValue:[[NSUserDefaults standardUserDefaults]valueForKey:@"set_AuthValue"] forHTTPHeaderField:@"Authorization"];
    NSLog(@"%@",strUrl);
    strUrl =[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"set_GetValue"]];
    [manager GET:strUrl parameters:nil progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        NSError* error;
        json = [NSJSONSerialization JSONObjectWithData:responseObject options:kNilOptions
                                                 error:&error];
        NSLog(@"Success %@",json);
        [[SingletonClasS sharedInstance]hideMBProgressHud:vc.view];
        
        completionBlock(json);
    }
     
         failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
             
             NSString *str = [[NSString alloc] initWithData:error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] encoding:NSUTF8StringEncoding];
             NSLog(@"Error ->%@", str);
             NSLog(@"error.description==%@", error.description);
             
             completionBlock(nil);
             [[SingletonClasS sharedInstance]hideMBProgressHud:vc.view];
             @try {
                 if (error.code == -1001){
                     [[SingletonClasS sharedInstance]showAlert:vc message:@"Internet connection is slow. Please try after sometime."];
                 }else if (error.code == -1005){
                     [[SingletonClasS sharedInstance]showAlert:vc message:@"The network connection was lost. Please try after sometime."];
                 }else if (error.code == 1009){
                     [[SingletonClasS sharedInstance]showAlert:vc message:@"The Internet connection appears to be offline."];
                 }
                 else if (error.code != -999){
                     [[SingletonClasS sharedInstance]showAlert:vc message:InternetFailureMessage];
                 }
                 
             } @catch (NSException *exception) {
                 NSLog(@"Unable to show pop up");
             }
             
         }];
}

#pragma mark- Handle Json response

-(BOOL)checkResponseForBool:(NSDictionary*)dict key:(NSString*)key{
    if ([dict valueForKey:key]){
        
        if ( [[json valueForKey:key] isKindOfClass:[NSString class]]){
            return [[json valueForKey:key] boolValue];
        }else if ( [[json valueForKey:key] isKindOfClass:[NSNumber class]]){
            return [[json valueForKey:key] boolValue];
        }
        return false;
    }
    return false;
}

-(NSArray*)checkResponseForArray:(NSDictionary*)dict key:(NSString*)key{
    if ([json valueForKey:key] && [[json valueForKey:key] isKindOfClass:[NSArray class]])
        return [json valueForKey:key];
    
    return [[NSArray alloc]init];
}

-(NSDictionary*)checkResponseForDict1:(NSDictionary*)dict
{
    NSLog(@"%@",json);
    
    return json;
    return [[NSDictionary alloc]init];
    
}

-(NSDictionary*)checkResponseForDict:(NSDictionary*)dict key:(NSString*)key{
    NSLog(@"%@",json);
    if ([json valueForKey:key] && [[json valueForKey:key] isKindOfClass:[NSDictionary class]])
        return [json valueForKey:key];
    
    return [[NSDictionary alloc]init];
}

#pragma mark-multipart

-(void)downloadImage:(UIImageView*)imgView urlStr:(NSString*)urlStr completionBlock:(voidCompletion)completionBlock{
    
    NSURL *url = [NSURL URLWithString:urlStr];
    [imgView sd_setImageWithURL:url placeholderImage:nil options: SDWebImageCacheMemoryOnly completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        
        if (imgView.subviews.count){
            @try {
                UIActivityIndicatorView *activityView = [[imgView subviews]objectAtIndex:0];
                [activityView removeFromSuperview];
            } @catch (NSException * e) {
                NSLog(@"Exception: %@", e);
                NSLog(@"Imageview dont have ActivityIndicatorView");
            }
        }
        
        if (image != nil){
            imgView.image = image;
        }
        completionBlock();
    }];
}

-(void)postMethodWithImage:(UIViewController*)vc url:(NSString *)strUrl dict:(NSDictionary *)dict completionBlock:(myCompletion)completionBlock :(NSData *)imageData
{
    
    if([SingletonClasS isInternetConnected]){
        [self hideNetworkView];
    }else{
        [[SingletonClasS sharedInstance]hideMBProgressHud:vc.view];
        [self showNetworkView];
        completionBlock(nil);
        return;
    }
    
    NSLog(@"dict %@",dict);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:strUrl parameters:dict constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         NSString *alphaNum = [NSString stringWithFormat:@"%@.jpg",[self randomStringWithLength:5]];
         if (imageData!=nil)
         {
             [formData appendPartWithFileData:imageData
                                         name:@"image"
                                     fileName:alphaNum mimeType:@"image/jpg"];
             
             //             [formData appendPartWithFormData:[key1 dataUsingEncoding:NSUTF8StringEncoding]
             //                                         name:@"key1"];
         }
         
     } progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
         // NSLog(@"Response: %@", responseObject);
         //[[SingletonClass sharedInstance]hideMBProgressHud:vc.view];
         
         BOOL status = [self checkResponseForBool:responseObject key:@"status"];
         if (status)
             completionBlock(responseObject);
         else{
             NSString *strMessage =[self checkResponseForString:responseObject key:@"message"];
             [self showAlert:vc message:strMessage];
             completionBlock(nil);
         }
         
     } failure:^(NSURLSessionDataTask *task, NSError *error) {
         completionBlock(nil);
         NSLog(@"%@",task.response);
         [[SingletonClasS sharedInstance]hideMBProgressHud:vc.view];
         if (error.code != -999){
             //[[SingletonClass sharedInstance]showAlert:vc message:FailureMessage];
         }//Request is cancelled explicitly
         
     }];
}

-(NSString *) randomStringWithLength: (int) len {
    NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    return randomString;
}

-(NSString *) saveSlotTimeString :(NSString *)strSlotTime
{
    NSMutableString *randomString;
    return randomString;
}


#pragma mark - date handlers
#pragma mark --
-(NSString *)convertDate:(NSString*)inputString{
    
    //Store your 24 hr time String format into string
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    NSDate *time = [dateFormatter dateFromString:inputString];
    
    // Convert time object into desired format
    [dateFormatter setDateFormat:@"hh:mm a"];
    return [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:time]];
}
-(NSString *)convertDateWithDay:(NSString *)dateStr
{
    //2017-04-04 02:40:00
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *orignalDate   =  [dateFormatter dateFromString:dateStr];
    
    [dateFormatter setDateFormat:@"dd MMM,EEE"];
    NSString *finalString = [dateFormatter stringFromDate:orignalDate];
    return finalString;
    
}
-(NSString *)convertDateDay:(NSString *)strDate
{
    //2017-04-04 02:40:00
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *orignalDate   =  [dateFormatter dateFromString:strDate];
    
    //[dateFormatter setDateFormat:@"MMM dd,EEEE hh:mm a"];
    [dateFormatter setDateFormat:@"MMM dd,EEEE"];
    NSString *finalString = [dateFormatter stringFromDate:orignalDate];
    return finalString;
    
}
#pragma mark - attibutted
-(NSAttributedString *)attributeName:(NSString*)fullText str:(NSString*)str color:(UIColor*)color{
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:fullText];
    
    if (attrStr.length == 0)
        return attrStr;
    
    NSDictionary *attrDict = @{
                               NSFontAttributeName : [UIFont systemFontOfSize:13],
                               NSForegroundColorAttributeName : color
                               };
    [attrStr addAttributes:attrDict range:NSMakeRange(fullText.length-str.length, str.length)];
    // [attrStr addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(fullText.length-str.length, str.length)];
    return attrStr;
}
-(NSAttributedString *)attributeTxt:(NSString*)fullText str:(NSString*)str color:(UIColor*)color{
    
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:fullText];
    ;
    if (str!=nil || str.length!=0) {
        NSRange range1 = [fullText rangeOfString:str];
        [attrStr addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:13] range:range1];
        [attrStr addAttribute:NSForegroundColorAttributeName value:color range:range1];
    }
   
    return attrStr;
}
-(NSAttributedString *)attributeTxtWithFont:(NSString*)fullText str:(NSString*)str color:(UIColor*)color : (UIFont *)ft{
    if (fullText != nil){
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:fullText];
        ;
        if (str!=nil || str.length!=0) {
            NSRange range1 = [fullText rangeOfString:str];
            [attrStr addAttribute:NSFontAttributeName value:ft range:range1];
            [attrStr addAttribute:NSForegroundColorAttributeName value:color range:range1];
        }
        
        return attrStr;
    }else{
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:@""];
        return attrStr;
    }
    
}

#pragma mark- DictionaryParameters

-(void)getDictParameterData:(NSArray *)keyValue passParameters:(NSArray *)new_PassParameterArray{
    @autoreleasepool {
        dictParameter = [[NSMutableDictionary alloc]init];
        
        for(int i=0;i<keyValue.count;i++)
        {
            [dictParameter setObject:new_PassParameterArray[i] forKey:keyValue[i]];
        }
        NSLog(@"%@",dictParameter);
    }
}

#pragma mark- API Methods Handling
-(void)handleApi1:(UIView*)myView1 title:(NSString*)strMessage vc: (UIViewController*)vc1 passStrUrl:(NSString*)stringUrl completionBlock:(myCompletion)completionBlock;
{
    [self showMBProgressHud:myView1 msg:strMessage];
    [self postMethod:vc1 url:stringUrl dict:dictParameter completionBlock:^(NSMutableDictionary *responseObject){
        NSLog(@"%@",responseObject);
        if (responseObject != nil)
        {
            userDetail= [[SingletonClasS sharedInstance]checkResponseForDict1:responseObject];
            
            if([[userDetail valueForKey:@"error"]boolValue]==1)
            {
                [[SingletonClasS sharedInstance]showAlert:vc1 message:CHECK_NULL_STRING([userDetail valueForKey:@"error_msg"])];
            }
            else
            {
                completionBlock(userDetail);
            }
          
            
        }
        
    }];
}
/*
 #pragma mark- API SignIn Modal
 -(NSMutableArray*)getSignInData:(NSArray *)arr{
 
 @autoreleasepool {
 NSMutableArray *objArr = [[NSMutableArray alloc]init];
 
 for (NSDictionary *dict in arr){
 SignInModalClass *obj = [self getSignInDetail:dict];
 [objArr addObject:obj];
 }
 return objArr;
 }
 }
 
 -(SignInModalClass*)getSignInDetail:(NSDictionary*)dict{
 @autoreleasepool {
 SignInModalClass *objSignIn = [[SignInModalClass alloc]init];
 objSignIn.email = [self checkResponseForString:dict key:@"email"];
 objSignIn.password = [self checkResponseForString:dict key:@"password"];
 return objSignIn;
 }
 }
 */
#pragma mark- MultipartImage
-(void)postMethodWithMultipleImage:(UIViewController*)vc url:(NSString *)strUrl dict:(NSDictionary *)dict completionBlock:(myCompletion)completionBlock :(NSMutableArray*)imgs
{
    
    if([SingletonClasS isInternetConnected]){
        [self hideNetworkView];
    }else{
        [[SingletonClasS sharedInstance]hideMBProgressHud:vc.view];
        [self showNetworkView];
        completionBlock(nil);
        return;
    }
    
    NSLog(@"dict %@",dict);
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager POST:strUrl parameters:dict constructingBodyWithBlock:^(id<AFMultipartFormData> formData)
     {
         
         for (int i=0; i<imgs.count; i++) {
             NSString *alphaNum = [NSString stringWithFormat:@"%@.jpg",[self randomStringWithLength:5]];
             UIImage *myImageObj = imgs[i];
             NSData *imageData = UIImageJPEGRepresentation(myImageObj, 0.5);
             if (imageData!=nil)
             {
                 [formData appendPartWithFileData:imageData
                                             name:@"image[]"
                                         fileName:alphaNum mimeType:@"image/jpg"];
                 
                 //             [formData appendPartWithFormData:[key1 dataUsingEncoding:NSUTF8StringEncoding]
                 //                                         name:@"key1"];
             }
         }
         
     } progress:nil success:^(NSURLSessionDataTask *task, id responseObject) {
         // NSLog(@"Response: %@", responseObject);
         //[[SingletonClass sharedInstance]hideMBProgressHud:vc.view];
         
         BOOL status = [self checkResponseForBool:responseObject key:@"status"];
         if (status)
             completionBlock(responseObject);
         else{
             NSString *strMessage =[self checkResponseForString:responseObject key:@"message"];
             [self showAlert:vc message:strMessage];
             completionBlock(nil);
         }
         
     } failure:^(NSURLSessionDataTask *task, NSError *error) {
         completionBlock(nil);
         NSLog(@"%@",task.response);
         [[SingletonClasS sharedInstance]hideMBProgressHud:vc.view];
         if (error.code != -999){
             //[[SingletonClass sharedInstance]showAlert:vc message:FailureMessage];
         }//Request is cancelled explicitly
         
     }];
}
/*
 -(NSMutableArray*)getReasonList:(NSArray *)arr{
 
 @autoreleasepool {
 NSMutableArray *objArr = [[NSMutableArray alloc]init];
 
 for (NSDictionary *dict in arr){
 ReasonObject *obj = [self getReasonDetail:dict];
 //if (obj.category_details.count)
 [objArr addObject:obj];
 }
 return objArr;
 }
 }
 
 -(ReasonObject*)getReasonDetail:(NSDictionary*)dict{
 
 @autoreleasepool {
 
 ReasonObject *obj = [[ReasonObject alloc]init];
 obj.idReason = [self checkResponseForString:dict key:@"id"];
 obj.reason = [self checkResponseForString:dict key:@"reason"];
 
 
 return obj;
 }
 
 }
 */
-(NSString*)checkStringValue:(NSString*)key{
    
    if([key isKindOfClass:[NSString class]]){
        return key;
    }else{
        return @"";
    }
    
}

-(void)getDictHome:(NSMutableDictionary *)dict_Home
{
    dict_SAVE_Home = [[NSMutableDictionary alloc]init];
    dict_SAVE_Home = dict_Home;
  
}
-(NSMutableDictionary *)dict_Save_HomeData
{
    return dict_SAVE_Home;
}
@end


