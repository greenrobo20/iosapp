

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//#import <FBSDKLoginKit/FBSDKLoginKit.h>
//#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <AFNetworking.h>
//#import "UserObj.h"
//#import "ScheduleBookingObject.h"
//#import "OwnerObjPark.h"
//#import "UserDetailIntegratedObject.h"
//#import "ReviewObject.h"
//#import "ProfileAndReview.h"
//#import "UserSignInModel.h"
@interface SingletonClasS : NSObject
{
   // FBSDKLoginManager *login;
    UIView *view1;
    NSMutableDictionary *dictParameter;
    NSMutableDictionary *userDetail;
    NSMutableArray *objArr_You,*objArr_Other_User , *objArr_Friend ,*objArr_Pfigure;
    NSMutableDictionary *dict_SAVE_Home;

}
+(SingletonClasS *)sharedInstance;
typedef void(^voidCompletion)();
typedef void(^voidSecCompletion)();

@property(nonatomic,strong)AFHTTPSessionManager *netRequest;
@property(nonatomic,strong)UILabel *networkView;

typedef void(^myCompletion)(id responseobject);
typedef void(^voidCompletion)();

+ (BOOL)isInternetConnected ;


- (BOOL) validateEmailWithString : (NSString*)email;
- (BOOL) isPasswordValid         : (NSString *)pwd ;
-(void)showAlertWithAction:(UIAlertController *) alert vc : (UIViewController*)vc message:(NSString*)msg completionBlock:(voidCompletion)completionBlock secCompletion : (voidSecCompletion)SecCompletion;
- (void) showSimpleAlert : (UIViewController*)vc message:(NSString*)msg completionBlock:(voidCompletion)completionBlock secCompletion : (voidSecCompletion)SecCompletion;

- (void) showAlert               : (UIViewController*)vc message:(NSString*)msg ;


-(void)getStoredValue;

-(void)initObjClass;


-(void)ActionSheetBlackBackgound:(UIViewController *)vic;
-(UINib*)getNibFile:(NSString*)nibName;

-(void)ActionSheetBlackBackgoundHide:(UIViewController *)vic;
@property (strong,nonatomic)NSMutableArray * photosImageArray;
@property (nonatomic,assign)NSInteger         userEnd;
@property (nonatomic,assign)NSInteger         userBtn;
@property (nonatomic,assign)BOOL             isOwner;
@property (nonatomic,assign)BOOL             settingVC;

#pragma mark- MBProgressHUD

-(void)showMBProgressHud:(UIView*)myView msg:(NSString*)msg;
-(void)hideMBProgressHud:(UIView*)myView;

#pragma mark- API
-(void)postMethod:(UIViewController*)vc url:(NSString *)url dict:(NSDictionary *)dict completionBlock:(myCompletion)completionBlock;
-(void)getMethod:(UIViewController*)vc url:(NSString *)strUrl dict:(NSDictionary *)dict completionBlock:(myCompletion)completionBlock;
-(void)postMethod1:(UIViewController*)vc url1:(NSString* )strUrl dict1:(NSDictionary* )dict completionBlock:(myCompletion)completionBlock;
-(void)cancelRequest;
- (void)result:(NSDictionary *)result;



#pragma mark- Json Response Handler

-(NSString*)checkForStoredValue:(NSString*)key;
-(NSString*)checkResponseForString:(NSDictionary*)dict key:(NSString*)key;
-(BOOL)checkResponseForBool:(NSDictionary*)dict key:(NSString*)key;
-(NSMutableArray*)checkResponseForArray:(NSDictionary*)dict key:(NSString*)key;
-(NSMutableDictionary*)checkResponseForDict:(NSDictionary*)dict key:(NSString*)key;
-(NSMutableDictionary*)checkResponseForDict1:(NSDictionary*)dict ;


#pragma mark - date
-(NSString *)convertDate:(NSString*)inputString;
-(NSString *)convertDateWithDay:(NSString *)dateStr;
-(NSString *)convertDateDay:(NSString *)strDate;
#pragma common
-(NSAttributedString *)attributeName:(NSString*)fullText str:(NSString*)str color:(UIColor*)color;
-(NSAttributedString *)attributeTxt:(NSString*)fullText str:(NSString*)str color:(UIColor*)color;
-(NSAttributedString *)attributeTxtWithFont:(NSString*)fullText str:(NSString*)str color:(UIColor*)color : (UIFont *)ft;



#pragma mark - api methods

-(void)handleApi1:(UIView*)myView1 title:(NSString*)strMessage vc: (UIViewController*)vc1 passStrUrl:(NSString*)stringUrl completionBlock:(myCompletion)completionBlock;
-(void)SignInParameter:(NSString*)strEmail str:(NSString*)strPassword;
-(void)getDictParameterData:(NSArray *)keyValue passParameters:(NSArray *)new_PassParameterArray;


-(NSMutableArray*)getSignInData:(NSArray *)arr;
-(NSMutableArray*)getSignUpData:(NSArray *)arr;


-(void)postMethodWithMultipleImage:(UIViewController*)vc url:(NSString *)strUrl dict:(NSDictionary *)dict completionBlock:(myCompletion)completionBlock :(NSMutableArray*)imgs;
-(NSString*)checkStringValue:(NSString*)key;
#pragma mark - api methods
-(NSMutableArray*)getCommentList_You:(NSArray *)arr;
-(NSMutableArray *)array_saveCommentObject_Data_You;

-(NSMutableArray*)getCommentList_OtherUser:(NSArray *)arr;
-(NSMutableArray *)array_saveCommentObject_Data_OtherUser;

-(NSMutableArray*)getCommentList_Friend:(NSArray *)arr;
-(NSMutableArray *)array_saveCommentObject_Data_Friend;

-(NSMutableArray*)getCommentList_Pfigure:(NSArray *)arr;
-(NSMutableArray *)array_saveCommentObject_Data_Pfigure;
-(void)getDictHome :(NSMutableDictionary *)dict_Home;
-(NSMutableDictionary *)dict_Save_HomeData;
#pragma mark- HideKeyboard
-(void)tap1:(UIView*)view;

@end

