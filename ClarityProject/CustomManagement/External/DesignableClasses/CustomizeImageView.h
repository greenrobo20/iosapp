//
//  CustomizeImageView.h
//  KrankParking
//
//  Created by MAC on 29/05/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface CustomizeImageView : UIImageView
@property(nonatomic) IBInspectable UIColor * borderColor;
@property(nonatomic) IBInspectable CGFloat   borderWidth;
@property(nonatomic) IBInspectable CGFloat   cornerRadius;
@property(nonatomic) IBInspectable CGPoint   shadow;

@end
