//
//  CustomizeImageView.m
//  KrankParking
//
//  Created by MAC on 29/05/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "CustomizeImageView.h"

@implementation CustomizeImageView

- (void) setBorderWidth:(CGFloat)borderWidth {
              _borderWidth  = borderWidth;
    self.layer.borderWidth  = borderWidth;
}
-(void)setBorderColor:(UIColor *)borderColor {
              _borderColor  = borderColor;
    self.layer.borderColor  = borderColor.CGColor;
}
- (void) setCornerRadius:(CGFloat)cornerRadius {
              _cornerRadius = cornerRadius;
    self.layer.cornerRadius = cornerRadius;
    self.clipsToBounds      = YES;
}
- (void)setShadow:(CGPoint)shadow{
               _shadow      = shadow;
    self.layer.shadowColor  = [UIColor lightGrayColor].CGColor;
    self.layer.shadowOpacity= 2;
    self.layer.shadowRadius = 3;
    self.layer.shadowOffset = CGSizeMake(1.0, 1.0);
    self.layer.masksToBounds= NO;
}

@end
