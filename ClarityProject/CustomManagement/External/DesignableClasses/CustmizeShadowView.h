//
//  CustmizeShadowView.h
//  KrankParking
//
//  Created by Vikram Raj Singh on 12/07/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface CustmizeShadowView : UIView
@property(nonatomic) IBInspectable UIColor * borderColor;
@property(nonatomic) IBInspectable CGFloat   borderWidth;
@property(nonatomic) IBInspectable CGFloat   cornerRadius;
@property(nonatomic) IBInspectable CGPoint   shadow;
@end
