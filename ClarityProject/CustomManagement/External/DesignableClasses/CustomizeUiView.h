//
//  CustomizeUiView.h
//  KrankParking
//
//  Created by MAC on 29/05/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface CustomizeUiView : UIView

@property(nonatomic) IBInspectable UIColor        * borderColor;
@property(nonatomic) IBInspectable CGFloat          borderWidth;
@property(nonatomic) IBInspectable CGFloat          cornerRadius;
@property(nonatomic) IBInspectable CGPoint          shadow;
@property(nonatomic,strong) IBInspectable UIColor * fillColor;
@property(nonatomic,strong) IBInspectable UIColor * strokeColor;

@end
