//
//  CustomizeUiView.m
//  KrankParking
//
//  Created by MAC on 29/05/17.
//  Copyright © 2017 MAC. All rights reserved.
//

#import "CustomizeUiView.h"

@implementation CustomizeUiView

- (void) setBorderWidth:(CGFloat)borderWidth {
              _borderWidth  = borderWidth;
    self.layer.borderWidth  = borderWidth;
}
-(void)setBorderColor:(UIColor *)borderColor {
              _borderColor  = borderColor;
    self.layer.borderColor  = borderColor.CGColor;
}
- (void) setCornerRadius:(CGFloat)cornerRadius {
              _cornerRadius = cornerRadius;
    self.layer.cornerRadius = cornerRadius;
    self.clipsToBounds      = YES;
}
- (void)setShadow:(CGPoint)shadow{
               _shadow      = shadow;
    self.layer.shadowColor  = [UIColor lightGrayColor].CGColor;
    self.layer.shadowOpacity= 2;
    self.layer.shadowRadius = 3;
    self.layer.shadowOffset = CGSizeMake(1.0, 1.0);
    self.layer.masksToBounds= NO;
}
- (void)drawRect:(CGRect)rect {
    
    //// Rectangle Drawing
    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius: 0];
    [self.fillColor setFill];
    [rectanglePath fill];
    [self.strokeColor setStroke];
    rectanglePath.lineWidth = 2;
    CGFloat rectanglePattern[] = {7, 2, 7, 2};
    [rectanglePath setLineDash: rectanglePattern count: 4 phase: 0];
    [rectanglePath stroke];
    [super drawRect:rect];
    
}


@end
