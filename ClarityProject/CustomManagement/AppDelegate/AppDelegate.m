//
//  AppDelegate.m
//  ClarityProject
//
//  Created by Gurpreet Singh on 23/10/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[NSUserDefaults standardUserDefaults]setValue:NULL forKey:@"set_GetValue"];
    
    if([[[NSUserDefaults standardUserDefaults]valueForKey:@"set_Passcode"]isEqualToString:@"1"])
    {
        LAContext *context = [[LAContext alloc] init];
        
        NSError *error = nil;
        if ([context canEvaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics error:&error]) {
            [context evaluatePolicy:LAPolicyDeviceOwnerAuthenticationWithBiometrics
                    localizedReason:@"Are you the device owner?"
                              reply:^(BOOL success, NSError *error) {
                                  dispatch_async(dispatch_get_main_queue(), ^{
                                      if (error) {
                                          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                          message:@"There was a problem verifying your identity."
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"Ok"
                                                                                otherButtonTitles:nil];
                                          [alert show];
                                          return;
                                      }
                                      
                                      if (success) {
                                          
                                          
                                          
//                                          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success"
//                                                                                          message:@"You are the device owner!"
//                                                                                         delegate:nil
//                                                                                cancelButtonTitle:@"Ok"
//                                                                                otherButtonTitles:nil];
//                                          [alert show];
                                          
                                          UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Success" message:@"You are the device owner!" preferredStyle:UIAlertControllerStyleAlert];
                                          UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                           
                                              ListOfProjectsViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ListOfProjectsViewController"]; //or the homeController
                                              UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
                                              self.window.rootViewController = navController;
                                              
                                              // Enter code here
                                          }];
                                          [alert1 addAction:defaultAction];
                                          
                                          // Present action where needed
                                          [self.window.rootViewController presentViewController:alert1 animated:YES completion:nil];
                                          
                                          
                                      } else {
                                          UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                                          message:@"You are not the device owner."
                                                                                         delegate:nil
                                                                                cancelButtonTitle:@"Ok"
                                                                                otherButtonTitles:nil];
                                          [alert show];
                                      }
                                  });
                              }];
            
        } else {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:@"Your device cannot authenticate using TouchID."
                                                           delegate:nil
                                                  cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
    }
        else
        {
            ViewController *loginController = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"ViewController"]; //or the homeController
            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:loginController];
            self.window.rootViewController = navController;
//            ViewController *view_cont =[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//            [self.navigationController pushViewController:view_cont animated:YES];
        }
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
