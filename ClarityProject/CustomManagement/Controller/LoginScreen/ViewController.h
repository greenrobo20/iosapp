//
//  ViewController.h
//  ClarityProject
//
//  Created by Gurpreet Singh on 23/10/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (nonatomic,strong)IBOutlet UITextField *textUserName;
@property (nonatomic,strong)IBOutlet UITextField *textPassword;
@property (nonatomic)IBOutlet UITextField *text_Type;

@property (nonatomic)IBOutlet UIButton *btnLogin;
@end

