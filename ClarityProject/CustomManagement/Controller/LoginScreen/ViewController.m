//
//  ViewController.m
//  ClarityProject
//
//  Created by Gurpreet Singh on 23/10/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *currencySymbol = [[NSLocale currentLocale] objectForKey:NSLocaleUsesMetricSystem];
    NSLog(@"%@",currencySymbol);
    
    
    NSString *language = NSBundle.mainBundle.preferredLocalizations.firstObject;
    
    NSLocale *locale = NSLocale.currentLocale;
    NSString *countryCode = [locale objectForKey:NSLocaleCountryCode];
    
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    NSString *country = [usLocale displayNameForKey:NSLocaleCountryCode
                                              value:countryCode];
    
    NSLog(@"country: %@", country);
    NSLog(@"%@",locale);
    
    
    
   
    
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewWillAppear:(BOOL)animated
{
      [[SingletonClasS sharedInstance]tap1:self.view];
     [self.navigationController setNavigationBarHidden:YES];
    
    
    UIView *paddingView1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
    self.text_Type.leftView = paddingView1;
  
    self.text_Type.leftViewMode = UITextFieldViewModeAlways;
    self.text_Type.textColor = [UIColor whiteColor];
    
    UIView *paddingView2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
    self.textUserName.leftView = paddingView2;
    
    self.textUserName.leftViewMode = UITextFieldViewModeAlways;
    self.textUserName.textColor = [UIColor whiteColor];
    
    
    UIView *paddingView3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
    self.textPassword.leftView = paddingView3;
    
    self.textPassword.leftViewMode = UITextFieldViewModeAlways;
    self.textPassword.textColor = [UIColor whiteColor];
    
    
    
    //self.text_Type.layer.borderColor=[[UIColor lightGrayColor]CGColor];
  //  self.text_ClientName.layer.cornerRadius = 5.0;
  //  self.text_ClientName.layer.borderWidth=1.0;
    
    
}
#pragma mark- textField
- (void)textFieldDidEndEditing:(UITextField *)textField{
    
    // [dropDown hideDropDown:sender];
    if (textField==_textUserName){
        
        [UIView animateWithDuration:0.5 animations:^{
            self.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);}];
        [textField resignFirstResponder];
    }
    else if (textField==_textPassword){
        [UIView animateWithDuration:0.5 animations:^{
            self.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);}];
        [textField resignFirstResponder];
    }
    else if (textField==_text_Type)
    {
        [UIView animateWithDuration:0.5 animations:^{
            self.view.frame = CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height);}];
        [textField resignFirstResponder];
    }
}
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    //  [dropDown hideDropDown:sender];
  
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{  //[dropDown hideDropDown:sender];
 
    if (textField==_textUserName)
    {
        [_textUserName becomeFirstResponder];
        [UIView animateWithDuration:0.5 animations:^{
            self.view.frame = CGRectMake(0, -50, self.view.bounds.size.width, self.view.bounds.size.height);}];
        
    }
    else if (textField==_textPassword)
    {
        [_textPassword becomeFirstResponder];
        [UIView animateWithDuration:0.5 animations:^{
            self.view.frame = CGRectMake(0, -70, self.view.bounds.size.width, self.view.bounds.size.height);}];
    }
    
    else if (textField==_text_Type)
    {
        [_text_Type becomeFirstResponder];
        [UIView animateWithDuration:0.5 animations:^{
            self.view.frame = CGRectMake(0, -120, self.view.bounds.size.width, self.view.bounds.size.height);
            
        }];
    }
}
#pragma mark- Validations
-(void)fillTextFiled {
    
    if (self.text_Type.text.length==0) {
        [self.text_Type becomeFirstResponder];
        self.text_Type.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Please enter environment." attributes:@{NSForegroundColorAttributeName: VALIDATION_COLOR_Red}];
        [self animateTextField];
    }
    
   else if (self.textUserName.text.length==0) {
        [self.textUserName becomeFirstResponder];
        self.textUserName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Please enter your username." attributes:@{NSForegroundColorAttributeName: VALIDATION_COLOR_Red}];
        [self animateTextField];
    }
    //    else if (![[SingletonClasS sharedInstance]validateEmailWithString:_textUserName.text]) {
    //        [self.textUserName becomeFirstResponder];
    //        self.textUserName.text=@"";
    //        self.textUserName.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Please enter valid email." attributes:@{NSForegroundColorAttributeName: VALIDATION_COLOR_Red}];
    //        [self animateTextField];
    //    }
    else if (self.textPassword.text.length==0) {
        [self.textPassword becomeFirstResponder];
        self.textPassword.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Please enter password." attributes:@{NSForegroundColorAttributeName: VALIDATION_COLOR_Red}];
        [self animateTextField];
    }
    
    else{
        
        //NSArray *arrayPassSignInData= @[CHECK_NULL_STRING([[NSUserDefaults standardUserDefaults]valueForKey:@"ADDED_BY"])];
        
        [[NSUserDefaults standardUserDefaults]setValue:_textUserName.text forKey:@"set_Username"];
        [[NSUserDefaults standardUserDefaults]setValue:_textPassword.text forKey:@"set_Password"];
        
      //  [[SingletonClasS sharedInstance]getDictParameterData:marketingHeadID passParameters:nil];
        [[SingletonClasS sharedInstance]handleApi1:self.view title:@"" vc:self passStrUrl:[NSString stringWithFormat:@"https://%@.ondemand.ca.com/ppm/rest/v1/auth/login",_text_Type.text] completionBlock:^(NSMutableDictionary *responseObject){
            NSLog(@"%@",responseObject);
            if(responseObject != nil)
            {
                if(responseObject.count>0)
                {
                    [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:@"set_Passcode"];
                    
                    UIAlertController *alert1 = [UIAlertController alertControllerWithTitle:@"Clarity Project" message:@"Do you want to enable passcode?" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *defaultAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        [[NSUserDefaults standardUserDefaults]setValue:@"1" forKey:@"set_Passcode"];
                        ListOfProjectsViewController *view_cont =[self.storyboard instantiateViewControllerWithIdentifier:@"ListOfProjectsViewController"];
                        [self.navigationController pushViewController:view_cont animated:YES];
                        // Enter code here
                    }];
                    UIAlertAction *defaultAction1 = [UIAlertAction actionWithTitle:@"NO" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        
                        [[NSUserDefaults standardUserDefaults]setValue:@"0" forKey:@"set_Passcode"];
                        ListOfProjectsViewController *view_cont =[self.storyboard instantiateViewControllerWithIdentifier:@"ListOfProjectsViewController"];
                        [self.navigationController pushViewController:view_cont animated:YES];
                        // Enter code here
                    }];
                    [alert1 addAction:defaultAction];
                     [alert1 addAction:defaultAction1];
                    
                    // Present action where needed
                    [self presentViewController:alert1 animated:YES completion:nil];
                    
                    
              
                    
                    
//                    array_dashboard = [[NSMutableArray alloc]init];
//                    array_dashboard = [[responseObject valueForKey:@"data"] mutableCopy];
//
//                    [_tbl_Client reloadData];
                    // [self.text_ClientName setItemList:[responseObject valueForKey:@"client_name"]];
                    //    [self.text_ClientName setItemList:[NSArray arrayWithObjects:@"London",@"Johannesburg",@"Moscow",@"Mumbai",@"Tokyo",@"Sydney", nil]];
                    
                }
                else
                {
                    
                }
            }
            
        }];
 
    }
    
        
 
}

#pragma mark-Animation
-(void)animateTextField{
    const int MAX_SHAKES = 6;
    const CGFloat SHAKE_DURATION = 0.05;
    const CGFloat SHAKE_TRANSFORM = 4;
    CGFloat direction = 1;
    for (int i = 0; i <= MAX_SHAKES; i++) {
        [UIView animateWithDuration:SHAKE_DURATION
                              delay:SHAKE_DURATION * i
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             if(self.text_Type.text.length==0)
                             {
                                 if (i >= MAX_SHAKES) {
                                     _text_Type.transform = CGAffineTransformIdentity;
                                 } else {
                                     _text_Type.transform = CGAffineTransformMakeTranslation(SHAKE_TRANSFORM * direction, 0);
                                 }
                             }
                             
                           else  if(self.textUserName.text.length==0)
                             {
                                 if (i >= MAX_SHAKES) {
                                     _textUserName.transform = CGAffineTransformIdentity;
                                 } else {
                                     _textUserName.transform = CGAffineTransformMakeTranslation(SHAKE_TRANSFORM * direction, 0);
                                 }
                             }
//                             else if(![[SingletonClasS sharedInstance]validateEmailWithString:_textUserName.text])
//                             {
//                                 if (i >= MAX_SHAKES) {
//                                     _textUserName.transform = CGAffineTransformIdentity;
//                                 } else {
//                                     _textUserName.transform = CGAffineTransformMakeTranslation(SHAKE_TRANSFORM * direction, 0);
//                                 }
//                             }
                             else if(self.textPassword.text.length==0)
                             {
                                 if (i >= MAX_SHAKES) {
                                     _textPassword.transform = CGAffineTransformIdentity;
                                 } else {
                                     _textPassword.transform = CGAffineTransformMakeTranslation(SHAKE_TRANSFORM * direction, 0);
                                 }
                             }
                             
                         } completion:nil];
        
        direction *= -1;
    }
}
#pragma mark- IBAction
-(IBAction)clickSignIn:(id)sender
{
    [self fillTextFiled];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
