//
//  Project_Detail_VC.h
//  ClarityProject
//
//  Created by Gurpreet Singh on 30/10/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Project_Detail_VC : UIViewController
@property(nonatomic)NSString *str_PassOverAllStatus;
@property(nonatomic)NSString *str_PassOverAllStatus_Color;
@property (weak, nonatomic) IBOutlet UILabel *LblProjectId;
@property (weak, nonatomic) IBOutlet UIButton *OverallBtn;
@property (weak, nonatomic) IBOutlet UIButton *scopeBtn;
@property (weak, nonatomic) IBOutlet UIButton *scheduleBtn;
@property (weak, nonatomic) IBOutlet UIButton *budgetBtn;
@property (weak, nonatomic) IBOutlet UILabel *LblManagerName;
@property (weak, nonatomic) IBOutlet UILabel *TxtStsatusUpdate;
@property (weak, nonatomic) IBOutlet UILabel *TxtKeyAccomplishment;
@property (weak, nonatomic) IBOutlet UILabel *TxtUpcomingActivities;

@property(nonatomic)NSMutableArray *array_pass_data;
@end
