//
//  Project_Detail_VC.m
//  ClarityProject
//
//  Created by Gurpreet Singh on 30/10/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import "Project_Detail_VC.h"

@interface Project_Detail_VC ()

@end

@implementation Project_Detail_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBarHidden=NO;
    [self.navigationController.navigationBar setBarTintColor:NAVIGATION_COLOR_NEED_HELP];
    self.navigationController.navigationItem.title = @"Projects";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:NAVIGATION_COLOR_NEED_HELP}];
    
    NSLog(@"%@",_array_pass_data);
    _LblManagerName.text = CHECK_NULL_STRING([[_array_pass_data valueForKey:@"manager"]valueForKey:@"displayValue"]);
    _LblProjectId.text = CHECK_NULL_STRING([_array_pass_data valueForKey:@"code"]);
    
    NSLog(@"%@",_array_pass_data);
    
    NSDictionary *result = [[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]mutableCopy];
    
    NSLog(@"%@",[result valueForKey:@"statusUpdate"]);
      NSLog(@"%lu",[[result valueForKey:@"statusUpdate"]count]);
    
   
    NSLog(@"%@",[[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"statusUpdate"]);
    
    NSLog(@"%lu",[[[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"statusUpdate"]count]);
    
    
    if ([[[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"statusUpdate"]objectAtIndex:0] == nil || [[[[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"statusUpdate"]objectAtIndex:0] isKindOfClass:[NSNull class]])
    {
        _TxtStsatusUpdate.text = @"No data available";
    }
    else
    {
        
        _TxtStsatusUpdate.text = [[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"statusUpdate"];
        
        
        
       
        
        
        
    }
    if([[[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"upcomingActivities"]objectAtIndex:0] == nil || [[[[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"upcomingActivities"]objectAtIndex:0] isKindOfClass:[NSNull class]])
    {
        _TxtUpcomingActivities.text = @"No data available";
    }
    else
    {
        _TxtUpcomingActivities.text = [[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"upcomingActivities"];
    }
    if([[[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"keyAccomplishments"]objectAtIndex:0] == nil || [[[[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"keyAccomplishments"]objectAtIndex:0] isKindOfClass:[NSNull class]])
    {
         _TxtKeyAccomplishment.text = @"No data available";
    }
    else
    {
        
         _TxtKeyAccomplishment.text = [[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"keyAccomplishments"];
    }
    
    
    if([[[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"costEffortStatus"]objectAtIndex:0] == nil || [[[[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"costEffortStatus"]objectAtIndex:0] isKindOfClass:[NSNull class]])
    {
        
    }
    else
    {
        [_budgetBtn setTitle:[[[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"costEffortStatus"]valueForKey:@"displayValue"] forState:UIControlStateNormal];
        
        
        if([_budgetBtn.titleLabel.text isEqualToString:@"On Track"])
        {
            [_budgetBtn setBackgroundColor:VALIDATION_COLOR_TRACK];
        }
        else if([_budgetBtn.titleLabel.text isEqualToString:@"Need Help"]|| [_budgetBtn.titleLabel.text isEqualToString:@"Needs Help"])
        {
            [_budgetBtn setBackgroundColor:VALIDATION_COLOR_NEED_HELP];
        }
        else if([_budgetBtn.titleLabel.text isEqualToString:@"At Risk"])
        {
            [_budgetBtn setBackgroundColor:VALIDATION_COLOR_RISK];
        }
        
       
    }

    
    if(self.str_PassOverAllStatus == nil || [self.str_PassOverAllStatus isKindOfClass:[NSNull class]])
    {
        
    }
    else
    {
        [_OverallBtn setTitle:self.str_PassOverAllStatus forState:UIControlStateNormal];
        
        if([self.str_PassOverAllStatus isEqualToString:@"On Track"])
        {
             [_OverallBtn setBackgroundColor:VALIDATION_COLOR_TRACK];
        }
       else if([self.str_PassOverAllStatus isEqualToString:@"Need Help"])
        {
            [_OverallBtn setBackgroundColor:VALIDATION_COLOR_NEED_HELP];
        }
       else if([self.str_PassOverAllStatus isEqualToString:@"At Risk"])
        {
            [_OverallBtn setBackgroundColor:VALIDATION_COLOR_RISK];
        }
    }
    
    
    if([[[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"scopeStatus"]objectAtIndex:0] == nil || [[[[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"scopeStatus"]objectAtIndex:0] isKindOfClass:[NSNull class]])
    {
        
    }
    else
    {
        [_scopeBtn setTitle:[[[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"scopeStatus"]valueForKey:@"displayValue"] forState:UIControlStateNormal];
        
        
        if([_scopeBtn.titleLabel.text isEqualToString:@"On Track"])
        {
            [_scopeBtn setBackgroundColor:VALIDATION_COLOR_TRACK];
        }
        else if([_scopeBtn.titleLabel.text isEqualToString:@"Need Help"]|| [_scopeBtn.titleLabel.text isEqualToString:@"Needs Help"])
        {
            [_scopeBtn setBackgroundColor:VALIDATION_COLOR_NEED_HELP];
        }
        else if([_scopeBtn.titleLabel.text isEqualToString:@"At Risk"])
        {
            [_scopeBtn setBackgroundColor:VALIDATION_COLOR_RISK];
        }
    }
    
    if([[[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"scheduleStatus"]objectAtIndex:0] == nil || [[[[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"scheduleStatus"]objectAtIndex:0] isKindOfClass:[NSNull class]])
    {
        
    }
    else
    {
        [_scheduleBtn setTitle:[[[[_array_pass_data valueForKey:@"projectStatusReports"]valueForKey:@"_results"]valueForKey:@"scheduleStatus"]valueForKey:@"displayValue"] forState:UIControlStateNormal];
        
        
        if([_scheduleBtn.titleLabel.text isEqualToString:@"On Track"])
        {
            [_scheduleBtn setBackgroundColor:VALIDATION_COLOR_TRACK];
        }
        else if([_scheduleBtn.titleLabel.text isEqualToString:@"Need Help"]|| [_scheduleBtn.titleLabel.text isEqualToString:@"Needs Help"])
        {
            [_scheduleBtn setBackgroundColor:VALIDATION_COLOR_NEED_HELP];
        }
        else if([_scheduleBtn.titleLabel.text isEqualToString:@"At Risk"])
        {
            [_scheduleBtn setBackgroundColor:VALIDATION_COLOR_RISK];
        }
        
    }
//
//    NSLog(@"%@",([_array_pass_data valueForKey:@"name"]));
//    NSLog(@"%@",([[result valueForKey:@"costEffortStatus"]valueForKey:@"displayValue"]));
//    NSLog(@"%@",([[result valueForKey:@"scheduleStatus"]valueForKey:@"displayValue"]));
//    NSLog(@"%@",([[result valueForKey:@"scopeStatus"]valueForKey:@"displayValue"]));
//    NSLog(@"%@",([result valueForKey:@"overallStatus"]));
//    NSLog(@"%@",([result valueForKey:@"statusUpdate"]));
//    NSLog(@"%@",([result valueForKey:@"keyAccomplishments"]));
//    NSLog(@"%@",([result valueForKey:@"upcomingActivities"]));
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
