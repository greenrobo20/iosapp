//
//  Project_DetailTableViewController.h
//  ClarityProject
//
//  Created by Gurpreet Singh on 24/10/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Project_DetailTableViewController : UITableViewController
{
    IBOutlet UIButton *btn_Need_Help , *btn_Scope , *btn_Schedule , *btn_Budget;
    IBOutlet UILabel *lbl_Status_Update , *lbl_Key_Accomplish , *lbl_Upcoming_Activities ,*lbl_ProjectID , *lbl_Manager;
}
@property(nonatomic)NSMutableArray *array_pass_data;
@end
