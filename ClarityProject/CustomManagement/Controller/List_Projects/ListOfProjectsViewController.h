//
//  ListOfProjectsViewController.h
//  ClarityProject
//
//  Created by Gurpreet Singh on 23/10/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListOfProjectsViewController : UIViewController
{
   // NSMutableArray *array_Data;
    NSString * searchStr;
    int  _currentPage,_totalPages;
    NSInteger kLoadingCellTag;
 
}
@property(nonatomic)IBOutlet UITextField *searchTextF;
@property (nonatomic)IBOutlet UITableView *tbl_ListOfProject;
@property (nonatomic)NSMutableArray *array_Data;
@property (nonatomic)NSMutableArray *array_Data_Search;
@property (nonatomic)NSMutableArray *array_save_Record;
@property (nonatomic)   NSString *str_Status;;
@end
