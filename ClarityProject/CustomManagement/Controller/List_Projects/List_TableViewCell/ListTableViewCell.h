//
//  ListTableViewCell.h
//  ClarityProject
//
//  Created by Gurpreet Singh on 23/10/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListTableViewCell : UITableViewCell
@property(nonatomic)IBOutlet UILabel *lblTitle;
@property(nonatomic)IBOutlet UILabel *lbl_name;
@property(nonatomic)IBOutlet UILabel *lbl_No;
@property(nonatomic)IBOutlet UILabel *lbl_StartDate;
@property(nonatomic)IBOutlet UILabel *lbl_EndDate;
@property(nonatomic)IBOutlet UIButton *btn_help;
@end
