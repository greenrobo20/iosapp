//
//  ListOfProjectsViewController.m
//  ClarityProject
//
//  Created by Gurpreet Singh on 23/10/18.
//  Copyright © 2018 Gurpreet Singh. All rights reserved.
//

#import "ListOfProjectsViewController.h"

@interface ListOfProjectsViewController ()

@end

@implementation ListOfProjectsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     _currentPage = 0;
    _totalPages = 0;
  
    
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 15, 15)];
    self.searchTextF.leftView = paddingView;
    self.searchTextF.leftViewMode = UITextFieldViewModeAlways;
    
    self.navigationController.navigationBarHidden=NO;
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController.navigationBar setBarTintColor:NAVIGATION_COLOR_NEED_HELP];
    self.navigationController.navigationItem.title = @"Projects";
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:NAVIGATION_COLOR_NEED_HELP}];
  
    self.array_Data_Search = [[NSMutableArray alloc]init];
    self.array_Data = [[NSMutableArray alloc]init];
    self.array_save_Record = [[NSMutableArray alloc]init];
    [self getListOfProjects];
    // Do any additional setup after loading the view.
}



-(void)getProjects
{
    if( self.array_Data.count>0)
    {
        [self.array_Data removeAllObjects];
    }
  //  https://cppm9267-dev.ondemand.ca.com/ppm/rest/v1/projects?fields=name,isActive,manager,code,budgetCostTotal,scheduleStart,scheduleFinish&sort=name asc&filter=(isActive = true) and (isTemplate = false) and (name  startsWith  'aa')&expand=(projectStatusReports=(fields=(code,createdDate,statusUpdate,reportStatus,costEffortStatus,scheduleStatus,scopeStatus,overallStatus,keyAccomplishments,upcomingActivities)))
    
    //NSString *str_new = @"https://cppm9267-dev.ondemand.ca.com/ppm/rest/v1/projects";
    NSString *str_new =[NSString stringWithFormat:@"https://cppm9267-dev.ondemand.ca.com/ppm/rest/v1/projects?fields=name,isActive,manager,code,budgetCostTotal,scheduleStart,scheduleFinish&sort=name asc&filter= (isActive = true) and (isTemplate = false) and (name startsWith '%@')&expand=(projectStatusReports=(fields=(code,createdDate,statusUpdate,reportStatus,costEffortStatus,scheduleStatus,scopeStatus,overallStatus,keyAccomplishments,upcomingActivities)))",searchStr];
    
    [[NSUserDefaults standardUserDefaults]setValue:str_new forKey:@"set_GetValue"];
    
    [[SingletonClasS sharedInstance]getMethod:self url:str_new dict:nil completionBlock:^(NSMutableDictionary *responseObject){
        NSLog(@"%@",responseObject);
        if([[responseObject valueForKey:@"_results"]count]>0)
        {
          
           
           
                self.array_Data = [[responseObject valueForKey:@"_results"]mutableCopy];
          
            
         
            
            
            [self.tbl_ListOfProject reloadData];
        }
        else
        {
            // _lbl_Price.text = @"0";
            //  _view_Price.hidden = YES;
        }
    }];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self.searchTextF resignFirstResponder];
    return YES;
}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
     searchStr = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
     if(searchStr.length>0)
     {
         [self getProjects];
     }
    else
    {
         [self getListOfProjects];
        //  [self.tbl_ListOfProject reloadData];
    }
    return true;
}

-(void)getListOfProjects
{
    if( self.array_Data.count>0)
    {
        [self.array_Data removeAllObjects];
    }
    //NSString *str_new = @"https://cppm9267-dev.ondemand.ca.com/ppm/rest/v1/projects";
    NSString *str_new =[NSString stringWithFormat:@"https://cppm9267-dev.ondemand.ca.com/ppm/rest/v1/projects?fields=name,isActive,manager,code,budgetCostTotal,scheduleStart,scheduleFinish&sort=name asc&filter= (isActive = true) and (isTemplate = false) &expand=(projectStatusReports=(fields=(code,createdDate,statusUpdate,reportStatus,costEffortStatus,scheduleStatus,scopeStatus,overallStatus,keyAccomplishments,upcomingActivities)))&offset=%d",0];

    [[NSUserDefaults standardUserDefaults]setValue:str_new forKey:@"set_GetValue"];
    
    [[SingletonClasS sharedInstance]getMethod:self url:str_new dict:nil completionBlock:^(NSMutableDictionary *responseObject){
        NSLog(@"%@",responseObject);
        if([[responseObject valueForKey:@"_results"]count]>0)
        {
          
        self.array_Data = [[responseObject valueForKey:@"_results"]mutableCopy];
      //  NSLog(@"%@",[[[responseObject valueForKey:@"_results"]valueForKey:@"projectStatusReports"]objectAtIndex:0]);
      
        //self->_totalPages = [[[[[responseObject valueForKey:@"_results"]valueForKey:@"projectStatusReports"]objectAtIndex:0]
                         //   objectForKey:@"_pageSize"]intValue];
           // NSLog(@"%@",[self.array_Data lastObject]);
        
            [self.tbl_ListOfProject reloadData];
        }
        else
        {
           // _lbl_Price.text = @"0";
          //  _view_Price.hidden = YES;
        }
    }];
}
#pragma mark - tableView

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
//    if (_currentPage == 0) {
        return self.array_Data.count;
//    }
    
//    if (_currentPage < _totalPages) {
//         return self.array_Data.count + 1;
//    }
         return 0;
   
  
}
- (UITableViewCell *)List_CellForIndexPath:(NSIndexPath *)indexPath {
  //  static NSString *cellIdentifier = @"cell";
    ListTableViewCell *cell = [_tbl_ListOfProject dequeueReusableCellWithIdentifier:@"Custom_Cell" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[ListTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Custom_Cell"];
    }
    
    
    
    
    cell.lblTitle.text = CHECK_NULL_STRING([[self.array_Data valueForKey:@"name"]objectAtIndex:indexPath.row]);
    cell.lbl_name.text =CHECK_NULL_STRING([[[self.array_Data valueForKey:@"manager"]valueForKey:@"displayValue"]objectAtIndex:indexPath.row]);
    //    if ([[[SingletonClasS sharedInstance].array_saveSearchObject_Data valueForKey:@"logo"]objectAtIndex:indexRating] == nil || [[[[SingletonClasS sharedInstance].array_saveSearchObject_Data valueForKey:@"logo"]objectAtIndex:indexRating] isKindOfClass:[NSNull class]])
    //    {
    if([[self.array_Data valueForKey:@"budgetCostTotal"]count]>0)
    {
        cell.lbl_No.hidden = NO;
        NSString *str_Currency = CHECK_NULL_STRING([[[self.array_Data valueForKey:@"budgetCostTotal"]valueForKey:@"currency"]objectAtIndex:indexPath.row]);
        
        NSString *str_Amt = [NSString stringWithFormat:@"%@",[[[self.array_Data valueForKey:@"budgetCostTotal"]valueForKey:@"amount"]objectAtIndex:indexPath.row]];
        
        
        
        double sevenpointthreefourfivesix = [str_Amt doubleValue];
        NSNumberFormatter * formatter11 = [[NSNumberFormatter alloc] init];
        [formatter11 setMaximumFractionDigits:0];
        NSLog(@"%@", [formatter11 stringFromNumber:[NSNumber numberWithDouble:sevenpointthreefourfivesix]]);
        
        
        
        
        NSInteger num_Amt = [[formatter11 stringFromNumber:[NSNumber numberWithDouble:sevenpointthreefourfivesix]] integerValue];
        
        NSLog(@"%@",[NSNumber numberWithInteger:num_Amt]);
        
        
        
        
        
        
        
        
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [formatter setLocale:[NSLocale currentLocale]];
        
        
        
        NSString *localizedMoneyString = [formatter stringFromNumber:[NSNumber numberWithInteger:num_Amt]];
        
        
        NSLog(@"%@",localizedMoneyString);
        
        localizedMoneyString = [localizedMoneyString substringToIndex:(localizedMoneyString.length - 3)];
        NSLog(@"%@",localizedMoneyString);
        
        cell.lbl_No.text =[NSString stringWithFormat:@"%@ %@",str_Currency,localizedMoneyString];
    }
    else
    {
        cell.lbl_No.hidden = YES;
    }
    
    
    NSString *dateString = [[self.array_Data valueForKey:@"scheduleStart"]objectAtIndex:indexPath.row];
    
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *datetime = [dateFormatter1 dateFromString:dateString];
    
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]]; // Prevent adjustment to user's local time zone.
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString* dateTimeInIsoFormatForZuluTimeZone = [dateFormatter stringFromDate:datetime];
    
    NSLog(@"%@",cell.lbl_StartDate.text);
    
    cell.lbl_StartDate.text = dateTimeInIsoFormatForZuluTimeZone;
    
    
    NSLog(@"%@",dateTimeInIsoFormatForZuluTimeZone);
    NSString *dateString1 = [[self.array_Data valueForKey:@"scheduleFinish"]objectAtIndex:indexPath.row];
    
    NSDateFormatter *dateFormatter11 = [[NSDateFormatter alloc] init];
    [dateFormatter11 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *datetime1 = [dateFormatter11 dateFromString:dateString1];
    
    
    NSDateFormatter* dateFormatter111 = [[NSDateFormatter alloc] init];
    [dateFormatter111 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]]; // Prevent adjustment to user's local time zone.
    
    [dateFormatter111 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString* dateTimeInIsoFormatForZuluTimeZone1 = [dateFormatter111 stringFromDate:datetime1];
    
    cell.lbl_EndDate.text = dateTimeInIsoFormatForZuluTimeZone1;
    
    
    
    
    NSLog(@"%ld",indexPath.row);
    
    
   // kLoadingCellTag = indexPath.row;
    
    
    if([[[[self.array_Data valueForKey:@"projectStatusReports"]objectAtIndex:indexPath.row]valueForKey:@"_results"]count]>0)
    {
        
        cell.btn_help.hidden = NO;
        NSInteger str_OverAllStatus =[[[[[[self.array_Data valueForKey:@"projectStatusReports"]objectAtIndex:indexPath.row]valueForKey:@"_results"]valueForKey:@"overallStatus"]objectAtIndex:0]integerValue];
        
        if(str_OverAllStatus >=0 && str_OverAllStatus < 40)
        {
            self.str_Status = @"On Track" ;
            [cell.btn_help setBackgroundColor:VALIDATION_COLOR_TRACK];
        }
        
        else if(str_OverAllStatus > 39 && str_OverAllStatus < 90)
        {
            self.str_Status = @"Need Help" ;
            [cell.btn_help setBackgroundColor:VALIDATION_COLOR_NEED_HELP];
        }
        
        else if(str_OverAllStatus > 89 && str_OverAllStatus <= 100)
        {
            self.str_Status = @"At Risk" ;
            [cell.btn_help setBackgroundColor:VALIDATION_COLOR_RISK];
        }
        [cell.btn_help setTitle: self.str_Status forState:UIControlStateNormal];
    }
    
    
    else
    {
        cell.btn_help.hidden = YES;
    }
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < self.array_Data.count) {
        return [self List_CellForIndexPath:indexPath];
    } else {
        kLoadingCellTag = indexPath.row;
       return [self List_CellForIndexPath:indexPath];
    }
}


- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (cell.tag == kLoadingCellTag) {
//        _currentPage++;
//        [self getListOfProjects];
//    }
}
/*
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"%ld",indexPath.row);
    
    ListTableViewCell *cell = [_tbl_ListOfProject dequeueReusableCellWithIdentifier:@"Custom_Cell" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[ListTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Custom_Cell"];
    }
    
    
    
    
    cell.lblTitle.text = CHECK_NULL_STRING([[self.array_Data valueForKey:@"name"]objectAtIndex:indexPath.row]);
    cell.lbl_name.text =CHECK_NULL_STRING([[[self.array_Data valueForKey:@"manager"]valueForKey:@"displayValue"]objectAtIndex:indexPath.row]);
//    if ([[[SingletonClasS sharedInstance].array_saveSearchObject_Data valueForKey:@"logo"]objectAtIndex:indexRating] == nil || [[[[SingletonClasS sharedInstance].array_saveSearchObject_Data valueForKey:@"logo"]objectAtIndex:indexRating] isKindOfClass:[NSNull class]])
//    {
    if([[self.array_Data valueForKey:@"budgetCostTotal"]count]>0)
    {
        cell.lbl_No.hidden = NO;
        NSString *str_Currency = CHECK_NULL_STRING([[[self.array_Data valueForKey:@"budgetCostTotal"]valueForKey:@"currency"]objectAtIndex:indexPath.row]);
        
        NSString *str_Amt = [NSString stringWithFormat:@"%@",[[[self.array_Data valueForKey:@"budgetCostTotal"]valueForKey:@"amount"]objectAtIndex:indexPath.row]];
        
        
        
        double sevenpointthreefourfivesix = [str_Amt doubleValue];
        NSNumberFormatter * formatter11 = [[NSNumberFormatter alloc] init];
        [formatter11 setMaximumFractionDigits:0];
        NSLog(@"%@", [formatter11 stringFromNumber:[NSNumber numberWithDouble:sevenpointthreefourfivesix]]);
        
        
        
        
        NSInteger num_Amt = [[formatter11 stringFromNumber:[NSNumber numberWithDouble:sevenpointthreefourfivesix]] integerValue];
        
        NSLog(@"%@",[NSNumber numberWithInteger:num_Amt]);
        
       
        
        
        
        
        
        
        NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
        [formatter setNumberStyle:NSNumberFormatterCurrencyStyle];
        [formatter setLocale:[NSLocale currentLocale]];
        
      
        
        NSString *localizedMoneyString = [formatter stringFromNumber:[NSNumber numberWithInteger:num_Amt]];
      
        
        NSLog(@"%@",localizedMoneyString);
        
      localizedMoneyString = [localizedMoneyString substringToIndex:(localizedMoneyString.length - 3)];
          NSLog(@"%@",localizedMoneyString);
        
         cell.lbl_No.text =[NSString stringWithFormat:@"%@ %@",str_Currency,localizedMoneyString];
    }
    else
    {
        cell.lbl_No.hidden = YES;
    }
    
  
    
    
    
 
    
    
    NSString *dateString = [[self.array_Data valueForKey:@"scheduleStart"]objectAtIndex:indexPath.row];
    
    
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc] init];
    [dateFormatter1 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *datetime = [dateFormatter1 dateFromString:dateString];

    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]]; // Prevent adjustment to user's local time zone.
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString* dateTimeInIsoFormatForZuluTimeZone = [dateFormatter stringFromDate:datetime];
   
    NSLog(@"%@",cell.lbl_StartDate.text);
    
    cell.lbl_StartDate.text = dateTimeInIsoFormatForZuluTimeZone;
    
    
    NSLog(@"%@",dateTimeInIsoFormatForZuluTimeZone);
    NSString *dateString1 = [[self.array_Data valueForKey:@"scheduleFinish"]objectAtIndex:indexPath.row];
    
    NSDateFormatter *dateFormatter11 = [[NSDateFormatter alloc] init];
    [dateFormatter11 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    NSDate *datetime1 = [dateFormatter11 dateFromString:dateString1];
    
    
    NSDateFormatter* dateFormatter111 = [[NSDateFormatter alloc] init];
    [dateFormatter111 setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]]; // Prevent adjustment to user's local time zone.
    
    [dateFormatter111 setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"];
    NSString* dateTimeInIsoFormatForZuluTimeZone1 = [dateFormatter111 stringFromDate:datetime1];
    
    cell.lbl_EndDate.text = dateTimeInIsoFormatForZuluTimeZone1;
    
    
    
    
    NSLog(@"%ld",indexPath.row);
  
    

    

    if([[[[self.array_Data valueForKey:@"projectStatusReports"]objectAtIndex:indexPath.row]valueForKey:@"_results"]count]>0)
    {

        cell.btn_help.hidden = NO;
     NSInteger str_OverAllStatus =[[[[[[self.array_Data valueForKey:@"projectStatusReports"]objectAtIndex:indexPath.row]valueForKey:@"_results"]valueForKey:@"overallStatus"]objectAtIndex:0]integerValue];

    if(str_OverAllStatus >=0 && str_OverAllStatus < 40)
    {
        self.str_Status = @"On Track" ;
        [cell.btn_help setBackgroundColor:VALIDATION_COLOR_TRACK];
    }

   else if(str_OverAllStatus > 39 && str_OverAllStatus < 90)
    {
         self.str_Status = @"Need Help" ;
         [cell.btn_help setBackgroundColor:VALIDATION_COLOR_NEED_HELP];
    }

   else if(str_OverAllStatus > 89 && str_OverAllStatus <= 100)
   {
        self.str_Status = @"At Risk" ;
        [cell.btn_help setBackgroundColor:VALIDATION_COLOR_RISK];
   }
    [cell.btn_help setTitle: self.str_Status forState:UIControlStateNormal];
    }
    
 
    else
    {
        cell.btn_help.hidden = YES;
    }
    return cell;
}
 */
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%ld",(long)indexPath.row);
//
    
        Project_Detail_VC *view_cont =[self.storyboard instantiateViewControllerWithIdentifier:@"Project_Detail_VC"];
    view_cont.array_pass_data = [self.array_Data objectAtIndex:indexPath.row];
    view_cont.str_PassOverAllStatus =  self.str_Status;
        [self.navigationController pushViewController:view_cont animated:YES];
   
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 122;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
